package com.kytms.system.dao.Impl;

import com.kytms.core.dao.impl.BaseDaoImpl;
import com.kytms.core.entity.LoginInfo;
import com.kytms.system.dao.LoginInfoDao;
import org.springframework.stereotype.Repository;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 臧英明
 *
 * @author
 * @create 2017-11-18
 */
@Repository(value = "LoginDao")
public class LoginInfoDaoImpl extends BaseDaoImpl<LoginInfo> implements LoginInfoDao<LoginInfo> {
}
