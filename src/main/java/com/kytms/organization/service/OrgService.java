package com.kytms.organization.service;

import com.kytms.core.entity.Organization;
import com.kytms.core.entity.User;
import com.kytms.core.model.CommModel;
import com.kytms.core.model.JgGridListModel;
import com.kytms.core.model.TreeModel;
import com.kytms.core.service.BaseService;

import java.util.List;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * @author 臧英明
 * @create 2017-11-23
 */
public interface OrgService<Organization> extends BaseService<Organization> {
    List<Organization> getOrgGrid(CommModel commModel);
    List<TreeModel> getZoneTree(String roleId, String id);
    void saveZone(String id,String ids);
    JgGridListModel getOrgList(CommModel commModel);
    List<com.kytms.core.entity.Organization> selectUserOrgs(User sessionUser);

    List<com.kytms.core.entity.Organization> getOrgTreeW(CommModel commModel);
}
